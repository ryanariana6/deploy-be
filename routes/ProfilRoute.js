import  express from "express";
import  {
    getProfil,
    getProfilById,
    createProfil,
    updateProfil,
    deleteProfil
} from "../controllers/Profil.js";

const router = express.Router();

router.get('/abouts',  getProfil);
router.get('/abouts/:id',  getProfilById);
router.post('/abouts', createProfil);
router.patch('/abouts/:id', updateProfil);
router.delete('/abouts/:id', deleteProfil);

export default router;