import  express from "express";
import  {
    getBerita,
    getBeritaById,
    createBerita,
    updateBerita,
    deleteBerita
} from "../controllers/Berita.js";


const router = express.Router();

router.get('/news', getBerita);
router.get('/news/:id', getBeritaById);
router.post('/news', createBerita);
router.patch('/news/:id', updateBerita);
router.delete('/news/:id', deleteBerita);

export default router;