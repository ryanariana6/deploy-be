import  express from "express";
import  {
    getAlat,
    getAlatById,
    createAlat,
    updateAlat,
    deleteAlat
} from "../controllers/Alat.js";


const router = express.Router();

router.get('/tools', getAlat);
router.get('/tools/:id', getAlatById);
router.post('/tools', createAlat);
router.patch('/tools/:id', updateAlat);
router.delete('/tools/:id', deleteAlat);

export default router;