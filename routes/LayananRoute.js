import  express from "express";
import  {
    getLayanan,
    getLayananById,
    createLayanan,
    updateLayanan,
    deleteLayanan
} from "../controllers/Layanan.js";

const router = express.Router();

router.get('/services', getLayanan);
router.get('/services/:id', getLayananById);
router.post('/services', createLayanan);
router.patch('/services/:id', updateLayanan);
router.delete('/services/:id', deleteLayanan);

export default router;