import express from 'express';
import fileUpload from "express-fileupload";
import cors from 'cors';
import path from 'path';
import session from 'express-session';
import dotenv from "dotenv";
import db from "./config/Database.js";
import SequelizeStore from "connect-session-sequelize";
import AdminRoute from "./routes/AdminRoute.js";
import BeritaRoute from "./routes/BeritaRoute.js";
import ProfilRoute from "./routes/ProfilRoute.js";
import LayananRoute from "./routes/LayananRoute.js";
import HeroRoute from "./routes/HeroRoute.js";
import AlatRoute from "./routes/AlatRoute.js";
import AuthRoute from "./routes/AuthRoute.js";
dotenv.config();

const app = express();

const sessionStore = SequelizeStore(session.Store);

const store = new sessionStore({
    db: db
});

app.on("error", function () {
    console.log(arguments)
});

// (async () => {
//     await db.sync();
// })();

store.sync();

app.use(session({
    secret: process.env.SESS_SECRET,
    resave: false,
    saveUninitialized: true,
    store: store,
    cookie: {
        sameSite: 'none', // Allow cross-domain cookies
        secure: 'auto'
    }
}))

app.use(cors({
    credentials: true,
    origin: ['http://localhost:3000', 'https://lpnshipyard.my.id']
}));
app.use(express.json());
app.use(express.static("public"));
app.use(fileUpload());
app.use(AdminRoute);
app.use(BeritaRoute);
app.use(ProfilRoute);
app.use(LayananRoute);
app.use(HeroRoute);
app.use(AlatRoute);
app.use(AuthRoute);

/* final catch-all route to index.html defined last */
app.get('/*', (req, res) => {
    res.sendFile(path.resolve('public/index.html'));
});

app.listen(process.env.APP_PORT, () => {
    console.log("server BACKEND up and running...");
});