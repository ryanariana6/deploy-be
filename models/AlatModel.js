import {Sequelize} from "sequelize";
import db from "../config/Database.js";
import Admin from "./AdminModel.js";

const {DataTypes} = Sequelize;

const Alat = db.define('alat',{
    uuidAlat : {
        type : DataTypes.STRING,
        defaultValue : DataTypes.UUIDV4,
        allowNullValues : false,
        validate : {
            notEmpty: true
        }
    },
    nameAlat : {
        type : DataTypes.STRING,
        allowNull : false,
        validate : {
            notEmpty: true
        }
    },
    imageAlat : {
        type : DataTypes.STRING,
        allowNull : false,
        validate : {
            notEmpty: true
        }
    },
    ketAlat : {
        type : DataTypes.TEXT,
        allowNull : false,
        validate : {
            notEmpty: true
        }
    },
    urlAlat : {
        type : DataTypes.STRING
    },
    userId : {
        type : DataTypes.TEXT,
        allowNull : true,
        validate : {
            notEmpty: false
        }
    },
}, {
    freezeTableName: true
});

Admin.hasMany(Alat);
Alat.belongsTo(Admin);

export default Alat;