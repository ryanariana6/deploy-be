import Berita from "../models/BeritaModel.js";
import Admin from "../models/AdminModel.js";
import { Op } from "sequelize";
import path from "path";
import fs from "fs";

export const getBerita = async (req, res) => {
    try {
        let response;
        response = await Berita.findAll({
            attributes: ['id', 'uuidBerita', 'nameBerita', 'imageBerita', 'ketBerita', 'urlBerita','userId', 'createdAt', 'updatedAt'],
            include: [{
                model: Admin
            }]
        });
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}

export const getBeritaById = async (req, res) => {
    try {
        console.log(req.params);
        const berita = await Berita.findOne({
            where: {
                id: req.params.id
            }
        });
        if (!berita) return res.status(404).json({ msg: "Data tidak ditemukan" });
        let response;
        response = await Berita.findOne({
            attributes: ['id', 'uuidBerita', 'nameBerita', 'imageBerita', 'ketBerita', 'urlBerita','userId', 'createdAt', 'updatedAt'],
            where: {
                id: berita.id
            },
            include: [{
                model: Admin
            }]
        });
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}

export const createBerita = async (req, res) => {
    if (req.files === null) return res.status(400).json({ msg: "No File Uploaded" });
    const name = req.body.title;
    const file = req.files.file;
    const keterangan = req.body.ketBerita;
    const uid = req.body.userId;
    const fileSize = file.data.length;
    const ext = path.extname(file.name);
    const fileName = file.md5 + ext;
    const url = `${req.protocol}://${req.get("host")}/images/imgBerita/${fileName}`;
    const allowedType = ['.png', '.jpg', '.jpeg'];

    if (!allowedType.includes(ext.toLowerCase())) return res.status(422).json({ msg: "Invalid Images" });
    if (fileSize > 5000000) return res.status(422).json({ msg: "Image must be less than 5 MB" });

    file.mv(`./public/images/imgBerita/${fileName}`, async (err) => {
        if (err) return res.status(500).json({ msg: err.message });
        try {
            await Berita.create({
                nameBerita: name,
                imageBerita: fileName,
                ketBerita: keterangan,
                urlBerita: url,
                userId: uid
            });
            res.status(201).json({ msg: "Berita Created Successfuly" });
        } catch (error) {
            res.status(500).json({ msg: error.message });
        }
    })
}

export const updateBerita = async (req, res) => {
    const berita = await Berita.findOne({
        where: {
            id: req.params.id
        }
    });
    if (!berita) return res.status(404).json({ msg: "no Data found" });
    let nama = "";
    let fileName = "";
    let kete = "";
    if (req.title === null) {
        nama = Berita.name;
        kete = Berita.ket;
    }
    if (req.files === null) {
        fileName = Berita.image;
        kete = Berita.ket;
    } else {
        const file = req.files.file;
        const fileSize = file.data.length;
        const ext = path.extname(file.name);
        fileName = file.md5 + ext;
        const allowedType = ['.png', '.jpg', '.jpeg'];

        if (!allowedType.includes(ext.toLowerCase())) return res.status(422).json({ msg: "Invalid Images" });
        if (fileSize > 5000000) return res.status(422).json({ msg: "Image must be less than 5 MB" });

        const filepath = `./public/images/imgBerita/${berita.imageBerita}`;
        fs.unlinkSync(filepath);

        file.mv(`./public/images/imgBerita/${fileName}`, (err) => {
            if (err) return res.status(500).json({ msg: err.message })
        });
    }
    const name = req.body.title;
    const keterangan = req.body.ketBerita;
    const url = `${req.protocol}://${req.get("host")}/images/imgNews/${fileName}`;
    try {
        await Berita.update({ nameBerita: name, imageBerita: fileName, ketBerita: keterangan, urlBerita: url }, {
            where: {
                [Op.and]: [{ id: berita.id }]
            }
        });
        res.status(200).json({ msg: "Berita updated successfully" });
    } catch (error) {
        console.log(error.message);
    }
}

export const deleteBerita = async (req, res) => {
    const berita = await Berita.findOne({
        where: {
            id: req.params.id
        }
    });
    if (!berita) return res.status(404).json({ msg: "no Data found" });
    const {nameBerita, imageBerita, ketBerita, urlBerita} = req.body;
        const filepath = `./public/images/imgBerita/${berita.imageBerita}`;
        fs.unlinkSync(filepath);
        try {
            await Berita.destroy({
            where: {
                id: berita.id
            }
        });
        
        res.status(200).json({ msg: "NEWS deleted successfully" });
    } catch (error) {
        console.log(error.message);
    }
}