import Profil from "../models/ProfilModel.js";
import Admin from "../models/AdminModel.js";
import {Op} from "sequelize";

export const getProfil = async (req, res)=> {
    try {
        let response;
        response = await Profil.findAll({
            attributes: ['id', 'uuid', 'sejarah', 'visi', 'misi', 'nilai', 'createdAt', 'updatedAt'],
            include: [{
                model: Admin
            }]
        });
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({msg: error.message});
    }
}

export const getProfilById = async (req, res)=> {
    try {
        const profil = await Profil.findOne({
            where:{
                uuid: req.params.id
            }
        });
        if(!profil) return res.status(404).json({msg: "Data tidak ditemukan"});
        let response;
        if(req.role === "admin"){
            response = await Profil.findOne({
                attributes:['uuid', 'sejarah','visi','misi','nilai','userId'],
                where:{
                    id: profil.id
                },
                include:[{
                    model: Admin
                }]
            });
        }else{
            response = await Profil.findOne({
                attributes:['uuid', 'sejarah','visi','misi','nilai','userId'],
                where:{
                    id: profil.id
                },
                include:[{
                    model: Admin
                }]
            });
        }
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({msg: error.message});
    }
}

export const createProfil = async (req, res)=> {
    const {sejarah, visi, misi, nilai, userId} = req.body;
    try {
        await Profil.create({
            sejarah: sejarah,
            visi: visi,
            misi: misi,
            nilai: nilai,
            userId: userId
        });
        res.status(201).json({msg: "Upload successfully"});
    } catch (error) {
        res.status(400).json({msg: error.message});
    }
}

export const updateProfil = async (req, res)=> {
    try {
        const profil = await Profil.findOne({
            where: {
                uuid: req.params.id
            }
        });
        if(!profil) return res.status(404).json({msg: "Data tidak ditemukan"});
        const {sejarah, visi, misi, nilai} = req.body;
        if(req.role === "admin"){
            await Profil.update({sejarah, visi, misi, nilai},{
                where:{
                    id: profil.id
                }
            });
        }else{
            await Profil.update({sejarah, visi, misi, nilai},{
                where:{
                    id: profil.id
                }
            });
        }
        res.status(200).json({msg: "Updated Successfully"});
    } catch (error) {
        res.status(400).json({msg: error.message});
    }
}

export const deleteProfil = async (req, res)=> {
    const profil = await Profil.findOne({
        where: {
            uuid: req.params.id
        }
        });
    if(!profil) return res.status(404).json({msg: "Data tidak ditemukan"});
    const {sejarah, visi, misi, nilai} = req.body;
    try {
        await Profil.destroy({
            where:{
                id: profil.id
            }
        });
    res.status(200).json({msg: "profil Delete"});
    } catch (error) {
        res.status(400).json({msg: error.message});
    }
}