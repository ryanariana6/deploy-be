import Alat from "../models/AlatModel.js";
import Admin from "../models/AdminModel.js";
import { Op } from "sequelize";
import path from "path";
import fs from "fs";

export const getAlat = async (req, res) => {
    try {
        let response;
        response = await Alat.findAll({
            attributes: ['id', 'uuidAlat', 'nameAlat', 'imageAlat', 'ketAlat', 'urlAlat', 'createdAt', 'updatedAt'],
            include: [{
                model: Admin
            }]
        });
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({msg: error.message});
    }
}

export const getAlatById = async (req, res) => {
    try {
        const alat = await Alat.findOne({
            where:{
                id: req.params.id
            }
        });
        if(!alat) return res.status(404).json({msg: "Data tidak ditemukan"});
        let response;
        response = await Alat.findOne({
            attributes: ['id', 'uuidAlat', 'nameAlat', 'imageAlat', 'ketAlat', 'urlAlat','userId', 'createdAt', 'updatedAt'],
            where: {
                id: alat.id
            },
            include: [{
                model: Admin
            }]
        });
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({msg: error.message});
    }
}

export const createAlat = async (req, res) => {
    if(req.files === null) return res.status(400).json({msg: "No File Uploaded"});
    const name = req.body.title;
    const file = req.files.file;
    const keterangan = req.body.ketAlat;
    const uid = req.body.userId;
    const fileSize = file.data.length;
    const ext = path.extname(file.name);
    const fileName = file.md5 + ext;
    const url = `${req.protocol}://${req.get("host")}/images/imgAlat/${fileName}`;
    const allowedType = ['.png','.jpg','.jpeg'];

    if(!allowedType.includes(ext.toLowerCase())) return res.status(422).json({msg: "Invalid Images"});
    if(fileSize > 5000000) return res.status(422).json({msg: "Image must be less than 5 MB"});

    file.mv(`./public/images/imgAlat/${fileName}`, async (err)=>{
        if(err) return res.status(500).json({msg: err.message});
        try {
            await Alat.create({
                nameAlat: name,
                imageAlat: fileName,
                ketAlat: keterangan,
                urlAlat: url,
                userId: uid
            });
            res.status(201).json({msg: " Created Successfuly"});
        } catch (error) {
            console.log(error);
            res.status(500).json({msg: error.message});
        }
    })
}

export const updateAlat = async (req, res) => {
    const alat = await Alat.findOne({
        where:{
            id : req.params.id
        }
    });
    if(!alat) return res.status(404).json({msg: "no Data found"});
    let nama = "";
    let fileName = "";
    let kete = "";
    if(req.title===null){
        nama = Alat.nameAlat;
        kete = Alat.ketAlat;
    }
    if(req.files === null){
        fileName = Alat.imageAlat;
        kete = Alat.ketAlat;
        // jika image kosong ID ADMIN upldate title tanpa update imagenya
    }else{
        const file = req.files.file;
        const fileSize = file.data.length;
        const ext = path.extname(file.name);
        fileName = file.md5 + ext;
        const allowedType = ['.png','.jpg','.jpeg'];

        if(!allowedType.includes(ext.toLowerCase())) return res.status(422).json({msg: "Invalid Images"});
        if(fileSize > 5000000) return res.status(422).json({msg: "Image must be less than 5 MB"});

        const filepath = `./public/images/imgAlat/${alat.imageAlat}`;
        fs.unlinkSync(filepath);

        file.mv(`./public/images/imgAlat/${fileName}`, (err)=>{
            if(err) return res.status(500).json({msg: err.message})
        });
    }
    const name = req.body.title;
    const keterangan = req.body.ketAlat;
    const url = `${req.protocol}://${req.get("host")}/images/imgAlat/${fileName}`;
    try {
        await Alat.update({nameAlat: name, imageAlat: fileName, ketAlat: keterangan, urlAlat: url}, {
            where: {
                [Op.and]:[{id: alat.id}]
            }
        });
        res.status(200).json({msg: "updated successfully"});
    } catch (error) {
        console.log(error.message);
    }
}

export const deleteAlat = async (req, res) => {
    const alat = await Alat.findOne({
        where:{
            id : req.params.id
        }
    });
    if(!alat) return res.status(404).json({msg: "no Data found"});
    const {nameAlat, imageAlat, ketAlat, urlAlat} = req.body;

        const filepath = `./public/images/imgAlat/${alat.imageAlat}`;
        fs.unlinkSync(filepath);
        try {
            await Alat.destroy({
            where: {
                id : alat.id
            }
        });

        res.status(200).json({msg: "Deleted successfully"});
    } catch (error) {
        console.log(error.message);
    }
}