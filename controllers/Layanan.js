import Layanan from "../models/LayananModel.js";
import Admin from "../models/AdminModel.js";
import {Op} from "sequelize";
import path from "path";
import fs from "fs";

export const getLayanan = async (req, res)=> {
    try {
        let response;
        response = await Layanan.findAll({
            attributes: ['id', 'uuidLayanan', 'nameLayanan', 'imageLayanan', 'ketLayanan', 'urlLayanan', 'createdAt', 'updatedAt'],
            include: [{
                model: Admin
            }]
        });
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({msg: error.message});
    }
}

export const getLayananById = async (req, res)=> {
    try {
        const layanan = await Layanan.findOne({
            where:{
                id: req.params.id
            }
        });
        if(!layanan) return res.status(404).json({msg: "Data tidak ditemukan"});
        let response;
        response = await Layanan.findOne({
            attributes: ['id', 'uuidLayanan', 'nameLayanan', 'imageLayanan', 'ketLayanan', 'urlLayanan','userId', 'createdAt', 'updatedAt'],
            where: {
                id: layanan.id
            },
            include: [{
                model: Admin
            }]
        });
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({msg: error.message});
    }
}

export const createLayanan = async (req, res)=> {
    if(req.files === null) return res.status(400).json({msg: "No File Uploaded"});
    const name = req.body.title;
    const file = req.files.file;
    const keterangan = req.body.ketLayanan;
    const uid = req.body.userId;
    const fileSize = file.data.length;
    const ext = path.extname(file.name);
    const fileName = file.md5 + ext;
    const url = `${req.protocol}://${req.get("host")}/images/imgLayanan/${fileName}`;
    const allowedType = ['.png','.jpg','.jpeg'];

    if(!allowedType.includes(ext.toLowerCase())) return res.status(422).json({msg: "Invalid Images"});
    if(fileSize > 5000000) return res.status(422).json({msg: "Image must be less than 5 MB"});

    file.mv(`./public/images/imgLayanan/${fileName}`, async (err)=>{
        if(err) return res.status(500).json({msg: err.message});
        try {
            await Layanan.create({
                nameLayanan: name,
                imageLayanan: fileName,
                ketLayanan: keterangan,
                urlLayanan: url,
                userId: uid
            });
            res.status(201).json({msg: "Layanan Created Successfuly"});
        } catch (error) {
            res.status(500).json({msg: error.message});
        }
    })
}

export const updateLayanan = async (req, res)=> {
    const layanan = await Layanan.findOne({
        where:{
            id : req.params.id
        }
    });
    if(!layanan) return res.status(404).json({msg: "no Data found"});
    let nama = "";
    let fileName = "";
    let kete = "";
    if(req.title===null){
        nama = Layanan.nameLayanan;
        kete = Layanan.ketLayanan;
    }
    if(req.files === null){
        fileName = Layanan.imageLayanan;
        kete = Layanan.ketLayanan;
    }else{
        const file = req.files.file;
        const fileSize = file.data.length;
        const ext = path.extname(file.name);
        fileName = file.md5 + ext;
        const allowedType = ['.png','.jpg','.jpeg'];

        if(!allowedType.includes(ext.toLowerCase())) return res.status(422).json({msg: "Invalid Images"});
        if(fileSize > 5000000) return res.status(422).json({msg: "Image must be less than 5 MB"});

        const filepath = `./public/images/imgLayanan/${layanan.imageLayanan}`;
        fs.unlinkSync(filepath);

        file.mv(`./public/images/imgLayanan/${fileName}`, (err)=>{
            if(err) return res.status(500).json({msg: err.message})
        });
    }
    const name = req.body.title;
    const keterangan = req.body.ketLayanan;
    const url = `${req.protocol}://${req.get("host")}/images/imgLayanan/${fileName}`;
    try {
        await Layanan.update({nameLayanan: name, imageLayanan: fileName, ketLayanan: keterangan, urlLayanan: url}, {
            where: {
                [Op.and]:[{id: layanan.id}]
            }
        });
        res.status(200).json({msg: "Layanan updated successfully"});
    } catch (error) {
        console.log(error.message);
    }
}

export const deleteLayanan = async (req, res)=> {
    const layanan = await Layanan.findOne({
        where:{
            id : req.params.id
        }
    });
    if(!layanan) return res.status(404).json({msg: "no Data found"});
    const {nameLayanan, imageLayanan, ketLayanan, urlLayanan} = req.body;

        const filepath = `./public/images/imgLayanan/${layanan.imageLayanan}`;
        fs.unlinkSync(filepath);
        try {
            await Layanan.destroy({
            where: {
                id : layanan.id
            }
        });

        res.status(200).json({msg: "Deleted successfully"});
    } catch (error) {
        console.log(error.message);
    }
}