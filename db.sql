-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 04 Apr 2024 pada 13.29
-- Versi server: 10.4.28-MariaDB
-- Versi PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `databaselpn`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `alat`
--

CREATE TABLE `alat` (
  `id` int(11) NOT NULL,
  `uuidAlat` varchar(255) DEFAULT NULL,
  `nameAlat` varchar(255) NOT NULL,
  `imageAlat` varchar(255) NOT NULL,
  `ketAlat` text NOT NULL,
  `urlAlat` varchar(255) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `alat`
--

INSERT INTO `alat` (`id`, `uuidAlat`, `nameAlat`, `imageAlat`, `ketAlat`, `urlAlat`, `userId`, `createdAt`, `updatedAt`) VALUES
(1, '3e64fcad-d9f3-406a-8a81-679fe3afd426', 'Crane 1 unit', '03e2fc549eee06a3b8be7a146e8764b0.jpg', 'Crane merupakan salah satu pesawat pengangkat dan pemindah material. Biasanya alat berat satu ini digunakan untuk memindahkan suatu barang dalam jumlah yang banyak dan berat. Alat satu ini memiliki bentuk yang panjang dan kemampuan mengangkat sangat kuat.', 'https://ryanariana6.my.id/images/imgAlat/03e2fc549eee06a3b8be7a146e8764b0.jpg', 2, '2024-03-25 16:23:25', '2024-03-25 16:23:25'),
(43, 'dcd9c0d1-4b5d-45a6-8b81-fc2ffd64ba63', 'Loader 1 unit', 'c1388429d195a19378ce701552cafdea.jpg', 'Loader Merupakan salah satu jenis alat berat yang dirancang khusus untuk memuat material seperti tanah, kerikil, batu, atau bahan konstruksi lainnya ke dalam truk atau tempat penyimpanan lainnya.', 'https://ryanariana6.my.id/images/imgAlat/undefined', 2, '2024-03-26 06:16:45', '2024-03-26 06:19:28'),
(44, '80c26f63-a602-41b9-888b-c7b646bb5d56', 'Excavator 1 unit', 'ebbcc8392d2398a506616f9c8bdea018.jpg', 'Excavator merupakan alat berat yang memiliki fungsi utama untuk menggali tanah dan memuatnya ke dalam truk atau menimbun tanah tersebut disekitar shovel dengan cara memutar badan excavator sampai dengan sudut 360°.', 'https://ryanariana6.my.id/images/imgAlat/ebbcc8392d2398a506616f9c8bdea018.jpg', 2, '2024-03-26 06:19:53', '2024-03-26 06:19:53'),
(45, '5f31eb28-da7d-4773-b0bf-9ed030a77033', 'Fork lift 1 unit', '2544c617c9c074f094c91dc5f78fd0d5.jpg', 'Forklift adalah alat bantu untuk mengangkat benda-benda berat dari satu tempat ke tempat yang lainnya, namun forklift biasanya hanya digunakan untuk jarak pendek seperti di dalam ruangan atau di dalam area pabrik saja.', 'https://ryanariana6.my.id/images/imgAlat/2544c617c9c074f094c91dc5f78fd0d5.jpg', 2, '2024-03-26 06:20:17', '2024-03-26 06:20:17'),
(46, 'eac9e629-9c94-47ae-9622-c0742bb34412', 'Compressor 7 unit', 'fa0f9d77f3a14550a537060e6a0ca7cd.jpeg', 'kompresor adalah mesin untuk memampatkan udara atau gas. Kompresor udara biasanya mengisap udara dari atmosfer. Namun ada pula yang mengisap udara atau gas yang bertekanan lebih tinggi dari tekanan atmosfer. Dalam hal ini kompresor bekerja sebagai penguat (booster).', 'https://ryanariana6.my.id/images/imgAlat/fa0f9d77f3a14550a537060e6a0ca7cd.jpeg', 2, '2024-03-26 06:20:36', '2024-03-26 06:20:36'),
(47, '8194c9df-b2cc-4080-8def-9c9f874de352', 'Genset (PLN) 2 unit', '29fe375c574a381072b1c635756b5cfd.jpg', 'Genset (singkatan dari bahasa Inggris: generator set) atau bahasa Indonesia: generator seterum) adalah mesin yang menggerakkan pembangkit listrik melalui motor bakar pembakaran dalam. Genset merupakan suatu alat yang dapat mengubah energi mekanik menjadi energi listrik.', 'https://ryanariana6.my.id/images/imgAlat/29fe375c574a381072b1c635756b5cfd.jpg', 2, '2024-03-26 06:20:54', '2024-03-26 06:20:54'),
(48, '83f60616-cf51-4136-a6f1-3e340c6bbddb', 'Trafo las 80 unit', '5f1161481a5716f492dae9f116636b67.jpg', 'Travo las adalah mesin yang digunakan untuk melakukan kegiatan pengelasan baik diluar ruangan maupun didalam ruangan, asalkan memiliki sumber listrik untuk menyalakan mesin travo las tersebut.', 'https://ryanariana6.my.id/images/imgAlat/5f1161481a5716f492dae9f116636b67.jpg', 2, '2024-03-26 06:21:14', '2024-03-26 06:26:09'),
(49, '423b801a-6588-45e3-a4b7-6700f18a1678', 'Airbag 30 pcs', 'feea9361f9b6b4881279aa705c5dd24c.jpg', 'Airbag laut sering disebut airbag peluncur kapal yang menunjukkan aplikasi paling populer dari airbag tugas berat ini untuk peluncuran kapal. Diperkirakan lebih dari 80 persen kapal baru yang dibangun dengan DWT di bawah 60.000 diluncurkan oleh kantung udara laut.', 'https://ryanariana6.my.id/images/imgAlat/feea9361f9b6b4881279aa705c5dd24c.jpg', 2, '2024-03-26 06:21:52', '2024-03-26 06:21:52'),
(50, 'a5dbd779-b600-492c-85b8-5663c916a88e', 'Winch 1 unit', 'e27429ea288ec4a8eb81353d7799ecf3.jpg', 'Towing winch adalah sejenis alat bantu dikapal yang berfungsi untuk penarikan beban berat yang dioperasikan dengan cara system control. Ini mengadopsi hidrolik bertekanan sebagai kekuatan untuk menggerakkan piston pompa hidrolik (power pack), sehingga dapat mengangkat dan menyeret muatan berat ke atas kapal.', 'https://ryanariana6.my.id/images/imgAlat/e27429ea288ec4a8eb81353d7799ecf3.jpg', 2, '2024-03-26 06:22:13', '2024-03-26 06:22:13'),
(51, '684520fc-bb89-4992-9e29-6fbd18bdc5e7', 'Plat Kapal Berbagai Ukuran', '62ec04f3fa8f685f4298830c1371f241.jpg', 'Plat kapal adalah plat yang memiliki spesifikasi yang sama persis dengan plat hitam. Plat kapal juga sering disebut sebagai ship plate atau marine plate. Umumnya, plat kapal tersedia dalam ukuran 5ft x 20ft dan 6ft x 20ft, ada juga produsen yang membuat ukuran 1500mm x 6000mm dan 1800mm x 6000mm. Toleransi plat kapal berstandar SNI kurang lebih berkisar antara 0,1mm s/d 0,3mm.', 'https://ryanariana6.my.id/images/imgAlat/62ec04f3fa8f685f4298830c1371f241.jpg', 2, '2024-04-04 11:14:59', '2024-04-04 11:14:59'),
(52, '8895b757-a8d4-4411-a4c4-1db83dd4f353', 'Besi Siku Berbagai Ukuran', 'a97ad111fc7d1ae0081a12c80a285e0f.jpg', 'Besi Siku adalah adalah potongan logam besi yang diletakan secara horizontal dengan sudut 90 derajat atau sudut siku-siku ke arah mesin cetak dan merupakan sebuah bar penopang yang terbuat dari baja galvanis dan sering sekali digunakan untuk las atau bor. Logam Besi ini juga disebut dengan nama lain angle bar, bar siku dan sering juga disebut sebagai “L-Bracket”. Bar siku atau angle bar atau bar siku digunakan untuk mengubah jaring ketika besi dimasukan dari samping atau langsung ke former folder, selain itu juga digunakan pada ribbon folder. Biasanya besi ini juga diisi dengan udara dan lubang untuk mengurangi gesekan pada jaring.', 'https://ryanariana6.my.id/images/imgAlat/a97ad111fc7d1ae0081a12c80a285e0f.jpg', 2, '2024-04-04 11:17:23', '2024-04-04 11:17:23'),
(53, '5bb0b696-b65c-469a-9416-859d5ec34a8c', 'Besi H-Beam Berbagai Ukuran', '9cadd805bfddd89b333468e95afa24a8.jpg', 'Besi H Beam, atau yang bisanya di sebut dengan balok H, adalah material utama dalam struktur baja yang digunakan dalam konstruksi bangunan. Dengan memiliki bentuk  penampang dan melintang seta menyerupai seperti huruf H. Besi H Beam mempunyai kekuatan dan stabilitas yang sangat baik.', 'https://ryanariana6.my.id/images/imgAlat/9cadd805bfddd89b333468e95afa24a8.jpg', 2, '2024-04-04 11:19:56', '2024-04-04 11:19:56'),
(54, 'af4e37b1-3002-48b5-9658-ba3c09bd017b', 'Besi As atau Round Bar ', '6c23847ffa4ba370399e7ce9535de2e1.jpg', 'Besi As/Round Bar adalah salah satu jenis besi berbentuk bulat panjang yang sangat kuat untuk kebutuhan industri dan kontruksi. Besi Round Bar biasanya digunakan sebagai kaki-kaki konstruksi untuk bagian yang di cor. Besi Assental disebut juga dengan istilah Shafting Bar atau Bar Stock. Besi bisa dibilang unsur logam yang paling banyak dan luas penggunaannya dalam kehidupan manusia.', 'https://ryanariana6.my.id/images/imgAlat/6c23847ffa4ba370399e7ce9535de2e1.jpg', 2, '2024-04-04 11:22:23', '2024-04-04 11:22:23'),
(55, 'b59b5690-c462-4171-861f-dcd2d05ef557', 'Cat Kapal Berbagai Warna', '6143930542ecc01ac1f981782a228b00.jpg', 'Fungsi dasar pengecatan adalah mencegah korosi. Korosi pada bangunan kapal menurunkan kualitas material. Plat-plat yang berkarat tebal, misalnya, kekuatannya akan berkurang. Jika karatnya kemudian rontok, bisa terbentuk lubang. Artinya, jaminan keselamatan terhadap muatan menurun.', 'https://ryanariana6.my.id/images/imgAlat/6143930542ecc01ac1f981782a228b00.jpg', 2, '2024-04-04 11:24:02', '2024-04-04 11:24:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id` int(11) NOT NULL,
  `uuidBerita` varchar(255) DEFAULT NULL,
  `nameBerita` varchar(255) NOT NULL,
  `imageBerita` varchar(255) NOT NULL,
  `ketBerita` text NOT NULL,
  `urlBerita` varchar(255) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id`, `uuidBerita`, `nameBerita`, `imageBerita`, `ketBerita`, `urlBerita`, `userId`, `createdAt`, `updatedAt`) VALUES
(1, '0b56ec67-3b77-4c57-a463-aa1169729bd1', 'Launcing Kapal TB. Permata 999 oleh Bapak Bupati Kab. Barru.', '2550413385edf6d165da40f1f863e5c9.jpeg', 'sabtu, 25 februari 2023 PT. Layar Perkasa Nusantara melakukan peresmian dan launcing kapal TB. Permata 999 yang di hadiri bapak Ir. H. Suardi Saleh, M.Si yangg kini menjabat sebagai wakil bupati Barru periode 2021-2024. PT. Layar Perkasa Nusantara, industri perkapalan dalam negeri, memproduksi sebuah kapal tunda atau kapal tug boat yang rampung hari ini. kapal tunda yang selesai yaitu TB. Permata 999 yang berada di area galangan PT. Layar Perkasa Nusantara kab Barru. hal ini di benarkan oleh bapak Asnawir selaku kapala Produksi, yang di temui hari sabtu, 25 februari 2023, beliau mengatakan, \"untuk kapal tunda atau tug boat, dari dulu indonesia telah membuat sendiri di galangan swasta atau negeri, dan kami sangat bersyukur karena peresmian dan launcing kapal TB. Permata 999 langsung di hadiri bapak Bupati Barru. semoga dengan keberadaan PT. Layar Perkasa Nusantara ini membuat industri perkalan dalam negeri bisa bersaing kedepannya.\"', 'https://ryanariana6.my.id/images/imgBerita/2550413385edf6d165da40f1f863e5c9.jpeg', 2, '2024-03-25 15:47:32', '2024-03-25 15:47:32'),
(2, '04992172-118c-4d91-bf95-3276c4117dde', 'Kedatangan General ASDP Cab. Bajoe di PT. LPN ', 'e0e5a9dbfeccfd061444f25c83b87fce.jpeg', 'Kedatangan general ASDP Cab. Bajoe, guna memantau aktivitas-aktivitas di PT. Layar Perkasa Nusantara kab. Barru, Hal ini di akui oleh Bapak Asnawir selaku kepala Produksi, yg di temui pada tanggal 03 Maret 2023, beliau mengatakan \" Kemarin bapak general ASDP Cab Bajoe memang datang ke kantor, beliau melihat-lihat perkembangan pelayanan dan fasilitas pelabuhan yang tetap harus terus berkualitas, serta beliau jg sangat berharap agar kehadiran PT ASDP ini slalu hadir melayani dan membantu konektivitas di wilayah Sulawesi Selatan\".', 'https://ryanariana6.my.id/images/imgNews/undefined', 2, '2024-03-25 15:48:07', '2024-03-25 16:01:12'),
(3, 'c2271aca-efce-4a66-a2d8-8c2067eca7a8', 'Kunjungan Wakil Bupati Kab. Barru beserta kepala Desa Batupute', '09242316fb7b11c214e542117a26b107.jpeg', 'Kamis, 03 Agustus 2023 PT. Layar Perkasa Nusantara Menerima kunjungan dari bapak Komisaris polisi (Purn) Aska Mappe yang kini menjabat sebagai wakil bupati Barru periode 2021-2024 dan di temani oleh kepala desa batu pute Bapak Jaharuddin.  Hal tersebut di benarkan oleh  Bapak Adit selaku kepala Galangan yg ditemui pada hari kamis 03 Agustus 2023.beliau mengatakan  \"Kami sangat senang, bangga dan merasa terhormat dengan ada nya kunjungan dari Bapak wakil bupati Barru dan Bapak kepala desa batupute ke kantor ini, karena dengan kedatangan beliau, itu membuktikan bahwa PT. Layar Perkasa Nusantara punya kualitas yang baik, semoga kedepan nya kami dan seluruh kru dari PT. Layar Perkasa Nusantara dapat terus berinovasi dan berkembang sesuai dgn perkembangan teknologi yg ada.\"', 'https://ryanariana6.my.id/images/imgNews/undefined', 2, '2024-03-25 15:48:40', '2024-03-25 16:03:01'),
(4, '148b1fce-bd60-46c0-ba4a-d6957d231d28', 'Kunjungan Anggota Dewan komisi sembilan untuk sosialisasi ketenagakerjaan.', 'da68f1bbd4796f841aa987586f66c105.jpeg', 'selasa, 20 februari 2024. PT Layar Perkasa Nusantara menerima kunjungan kerja dari Anggota Dewan Komisi IX untuk melakukan sosialisasi Ketenagakerjaan. pada kesempatan kunjungan kerja hari itu, anggota Dewan komisi IX menyampaikan tentang ketenagakerjaan. Di Kabupaten Barru sudah diberikan program pendukung bagi kepesertaan BPJS Ketenagakejaan dalam bentuk sosialisasi dan pembinaan jaminan sosial serta kesejahteraan pekerja. hal itu di benarkan oleh bapak Asnawir selaku kepala Produksi,\" Memang benar kami kedatang Anggota Dewan komisi IX di PT. Layar Perkasa Nusantara, dan Kami bertemikasih kepada Anggota Dewan Komisi IX yang datang melakukan sosialisasi serta memberikan dorongan kepada Kementerian Tenaga Kerja untuk memberikan supportnya kepada Kabupaten Barru. Kedepannya kami masih sangat berharap perhatian dukungan dan dorongan support dari Anggota Dewan komisi IX dan kemerntrian Tenaga Kerja.\"', 'https://ryanariana6.my.id/images/imgBerita/da68f1bbd4796f841aa987586f66c105.jpeg', 2, '2024-03-25 15:49:05', '2024-03-25 15:49:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `heros`
--

CREATE TABLE `heros` (
  `id` int(11) NOT NULL,
  `uuidHero` varchar(255) DEFAULT NULL,
  `nameHero` varchar(255) NOT NULL,
  `imageHero` varchar(255) NOT NULL,
  `ketHero` text NOT NULL,
  `urlHero` varchar(255) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `heros`
--

INSERT INTO `heros` (`id`, `uuidHero`, `nameHero`, `imageHero`, `ketHero`, `urlHero`, `userId`, `createdAt`, `updatedAt`) VALUES
(1, '9c5416b7-3aad-4df4-9ad9-9023bfd94da2', 'PT. LAYAR PERKASA NUSANTARA', '208bb0575452b5009d459aa025a6648f.jpg', 'Pekerja PT. Layar Perkasa Nusantara memiliki sertifikasi dan kualifikasi sesuai bidang Perkapalan.', 'https://ryanariana6.my.id/images/imgHero/208bb0575452b5009d459aa025a6648f.jpg', 2, '2024-03-25 15:30:32', '2024-03-25 15:30:32'),
(2, 'd1a2f10c-9d0a-450b-b53c-21fbea71aa18', 'PT. LAYAR PERKASA NUSANTARA', 'fcde93e22e25e5ed931a719bb6ebb6c4.jpeg', 'Prioritas untuk selalu berkerja Profesional Serta Pentingnya Teknologi Terbaru demi Kepuasan Anda.', 'https://ryanariana6.my.id/images/imgHero/fcde93e22e25e5ed931a719bb6ebb6c4.jpeg', 2, '2024-03-25 15:30:56', '2024-03-25 15:30:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `layanans`
--

CREATE TABLE `layanans` (
  `id` int(11) NOT NULL,
  `uuidLayanan` varchar(255) DEFAULT NULL,
  `nameLayanan` varchar(255) NOT NULL,
  `imageLayanan` varchar(255) NOT NULL,
  `ketLayanan` text NOT NULL,
  `urlLayanan` varchar(255) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `layanans`
--

INSERT INTO `layanans` (`id`, `uuidLayanan`, `nameLayanan`, `imageLayanan`, `ketLayanan`, `urlLayanan`, `userId`, `createdAt`, `updatedAt`) VALUES
(1, '8a6b39ac-629d-4a60-a38f-1a1949e52bef', 'Fasilitas dan Peralatan di Galangan Kapal.', '3a52dd9d1636c7ced76ef1af93d4045c.png', '<p> Galangan kapal kami memiliki fasilitas dan peralatan seperti slipway, airbag, jetty building berth, gudang, bengkel, crane, forklift, tugboat dan excavator. Ini digunakan untuk meluncurkan, mengangkut, tambat, melayani menyimpan, dan membangun kapal. Layanan umum seperti utilitas dan pengelolaan limbah juga disediakan. Standar dan peraturan keselamatan diikuti secara ketat untuk memastikan keselamatan pekerja dan lingkungan.<p> Area Docking and Building Berth.<p> 1. Dock 125 m panjang x 30 m lebar.<p> 2. Dock 125 m panjang x 30 m lebar.<p> 3. Dock 125 m panjang x 30 m lebar.<p> 4. Dock 125 m panjang x 30 m lebar.<p> 5. Dock 125 m panjang x 25 m lebar.', 'https://ryanariana6.my.id/images/imgLayanan/3a52dd9d1636c7ced76ef1af93d4045c.png', 2, '2024-03-25 15:41:45', '2024-03-25 15:41:45'),
(2, '08467cf1-3ef0-4a89-ae8d-6e231b1332c3', 'Proses Docking dan Undocking.', '163b4b22851fa2d3794adc90a34c7c28.png', '<p> Proses docking dan undocking menggunakan air-bag slipway merupakan suatu metode perpindahan kapal antara air dan darat dengan airbag sebagai elemen pendukung.<p> Prosesnya melibatkan manuver kapal ke lereng atau tanjakan yang mengarah ke air dan menempatkan kantung udara di bawah lambung untuk mengangkatnya keluar dari air dan ke tempat peluncuran kapal.<p> Prosesnya memerlukan perencanaan yang cermat, operator yang terampil, dan teknisi untuk memantau dan menyesuaikan tekanan airbag.<p> Jalur peluncuran airbag adalah alternatif yang hemat biaya dan ramah lingkungan dibandingkan dok kering tradisional, tetapi kesesuaian metode ini bergantung pada ukuran dan jenis kapal.', 'https://ryanariana6.my.id/images/imgLayanan/163b4b22851fa2d3794adc90a34c7c28.png', 2, '2024-03-25 15:42:14', '2024-03-25 15:42:14'),
(3, '96fcbed8-7412-46d1-a316-655d320d23d6', 'Proses Rekayasa di Galangan Kapal.', 'bd862d351aae0947f1a41717ec6a7bd6.png', '<p> Proses teknik di galangan kapal melibatkan beberapa tahap, seperti desain, perencanaan, dan konstruksi. Ini juga meliputi pemasangan dan pengujian berbagai sistem, seperti sistem propulsi, pembangkit listrik, dan sistem listrik dan hidrolik.<p> Proses ini membutuhkan keahlian insinyur, teknisi, dan pekerja terampil yang bekerja sama untuk memastikan kapal dibangun sesuai spesifikasi dan memenuhi standar keselamatan.<p> Proses ini dipantau dan diperiksa pada berbagai tahap untuk memastikan kepatuhan dengan peraturan dan untuk mengidentifikasi masalah yang mungkin timbul.', 'https://ryanariana6.my.id/images/imgLayanan/bd862d351aae0947f1a41717ec6a7bd6.png', 2, '2024-03-25 15:42:39', '2024-03-25 15:42:39'),
(4, 'b40a50b9-f991-400e-b80a-ef8b927bb414', 'Proses Hotwork dan Replating di Galangan.', 'bd862d351aae0947f1a41717ec6a7bd6.png', '<p> Hot work atau replating di galangan adalah proses perbaikan atau penggantian material pada bagian kapal yang rusak atau aus. Proses ini melibatkan penggunaan mesin pemotong, las, dan bahan-bahan pemanas.<p> Semua pekerjaan hot work atau replating dilakukan dengan hati-hati dan memenuhi standar keselamatan yang ketat untuk mencegah kebakaran atau ledakan.', 'https://ryanariana6.my.id/images/imgLayanan/bd862d351aae0947f1a41717ec6a7bd6.png', 2, '2024-03-25 15:43:03', '2024-03-25 15:43:03'),
(5, 'ce297540-770d-45a1-89d0-562b8a3c2809', 'Proses Kontrol Kualitas dalam Proses Produksi.', '4b4daf38ac2461961908c73512397dfc.png', '<p> Quality control merupakan suatu proses yang dilakukan dalam proses produksi untuk mendapatkan standar mutu yang diinginkan.<p> Proses Quality Control dimulai dari tahap perancangan gambar, pemilihan bahan material, tahap produksi, hingga pengukuran sesuai standar yang diterapkan.<p> Hal ini dilakukan untuk menjamin konstruksi dan instalasi kapal yang aman saat beroperasi atau melakukan pelayaran.', 'https://ryanariana6.my.id/images/imgLayanan/4b4daf38ac2461961908c73512397dfc.png', 2, '2024-03-25 15:43:27', '2024-03-25 15:43:27'),
(6, 'a993bc2d-7540-4c42-b727-8d1caa70422e', 'Pekerjaan Sistem Propulsi Kapal.', '0079fc714f505829ac6acdfa60b22b88.png', '<p> Propulsi kapal sangat penting untuk menggerakkan kapal. Saat terjadi kerusakan pada bagian propulsi seperti shaft, propeller, dan kemudi, maka perlu dilakukan perbaikan atau penggantian.<p> Proses perbaikan meliputi bongkar, perbaiki propeller, seimbangkan propeller, dan pasang kembali semua bagian dengan hati-hati dan memenuhi standar keselamatan yang ketat.<p> Proses perbaikan propulsi kapal penting untuk memastikan kapal dapat berlayar dengan aman dan efektif.', 'https://ryanariana6.my.id/images/imgLayanan/0079fc714f505829ac6acdfa60b22b88.png', 2, '2024-03-25 15:43:46', '2024-03-25 15:43:46'),
(7, '474138e4-d9f4-42b2-8a55-9d9af5b0687e', 'Pekerjaan Perawatan dan Perbaikan Mesin Kapal.', '0079fc714f505829ac6acdfa60b22b88.png', '<p> Proses perbaikan dan perawatan mesin induk kapal sangat penting untuk memastikan mesin beroperasi dengan baik dan mencegah kerusakan yang lebih serius.<p> Proses ini meliputi pembersihan, penggantian suku cadang yang rusak, pengecekan dan pengaturan tekanan serta suhu mesin, dan penggantian oli dan filter.<p> Selain itu, dilakukan juga pengecekan pada sistem bahan bakar, sistem pendingin, dan sistem pelumasan.<p> Proses perawatan dan perbaikan mesin induk kapal harus dilakukan secara teratur dan memenuhi standar keselamatan yang ketat untuk memastikan keselamatan seluruh awak kapal dan keberhasilan pelayaran.', 'https://ryanariana6.my.id/images/imgLayanan/0079fc714f505829ac6acdfa60b22b88.png', 2, '2024-03-25 15:44:11', '2024-03-25 15:44:11'),
(8, '74f69080-7a80-489f-8618-3a50d32697e8', 'Perkerjaan Elektrikal.', 'e076f92bedb8a9abafdbc2ebff3bf118.png', '<p> Pekerjaan listrik yang dilakukan di Galangan LPN Shipyard meliputi shore connection, pengujian megger, perbaikan dan pemeliharaan generator kapal dan lampu.<p> Shore connection penting untuk menghubungkan kapal dengan sumber listrik darat, sedangkan pengujian listrik dilakukan untuk memastikan sistem listrik kapal berfungsi dengan baik.<p> Semua pekerjaan dilakukan dengan hati-hati dan mematuhi standar keselamatan yang ketat.', 'https://ryanariana6.my.id/images/imgLayanan/e076f92bedb8a9abafdbc2ebff3bf118.png', 2, '2024-03-25 15:44:32', '2024-03-25 15:44:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profils`
--

CREATE TABLE `profils` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `sejarah` text NOT NULL,
  `visi` text NOT NULL,
  `misi` text NOT NULL,
  `nilai` text NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `profils`
--

INSERT INTO `profils` (`id`, `uuid`, `sejarah`, `visi`, `misi`, `nilai`, `userId`, `createdAt`, `updatedAt`) VALUES
(2, '7794e09c-c821-497c-97ac-0e4ff6066b6e', '<p> PT. Layar Perkasa Nusantara didirikan sebagai perusahaan pelayaran pada tahun 2012. Sejak itu, perusahaan telah memperluas layanannya untuk mencakup teknik mesin, sipil dan perbaikan kapal pada tahun 2014, dan kemudian terdaftar sebagai badan hukum pada tahun 2020. Galangan kapal ini berlokasi di Barru, provinsi Sulawesi Selatan, dan memiliki keahlian dalam arsitektur kapal serta seluruh aspek operasi kelautan, termasuk desain dan modifikasi segala jenis kapal, perbaikan kapal, dan overhaul mesin.<p> PT. Layar Perkasa Nusantara berkomitmen untuk menyediakan solusi kelautan berkualitas tinggi yang bersaing secara harga dan dikirim tepat waktu. Oleh karena itu, PT. Layar Perkasa Nusantara siap untuk memenuhi kebutuhan kapal baru di Indonesia, dengan komitmen yang kuat terhadap kualitas yang sangat baik, harga yang bersaing, dan pengiriman yang andal.', 'Menjadi salah satu galangan kapal terkemuka di Indonesia dalam hal kualitas, inovasi, dan layanan pelanggan.', '<p> 1. Memberikan produk dan layanan yang berkualitas tinggi dan inovatif kepada pelanggan kami. <p> 2. Mengembangkan dan mempertahankan standar keselamatan dan kesehatan kerja yang tinggi di seluruh operasi kami. <p> 3. Memastikan keberlanjutan lingkungan dan meminimalkan dampak operasi kami pada lingkungan. <p> 4. Meningkatkan keterampilan dan pengetahuan karyawan kami melalui pelatihan dan pengembangan. <p> 5. Menjalin hubungan jangka panjang dengan pelanggan, pemasok, dan mitra bisnis kami.', '<p> 1. KUALITAS. Mengutamakan kualitas dalam setiap aspek operasi kami. <p>2. KEAMANAN. Menempatkan keselamatan karyawan dan pelanggan sebagai prioritas utama. <p>3. INOVASI. Terus mendorong inovasi dan adopsi teknologi baru. <p>4. KEBERLANJUTAN. Mengambil tanggung jawab sosial dan lingkungan yang berkelanjutan. <p>5. INTEGRITAS. Bertindak dengan integritas dan transparansi dalam setiap aspek bisnis kami.', 2, '2024-03-26 03:07:16', '2024-03-26 03:07:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sessions`
--

CREATE TABLE `sessions` (
  `sid` varchar(36) NOT NULL,
  `expires` datetime DEFAULT NULL,
  `data` text DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `sessions`
--

INSERT INTO `sessions` (`sid`, `expires`, `data`, `createdAt`, `updatedAt`) VALUES
('GnjDs6VNJtve6SJFHpFfY4g-9Os5gxEk', '2024-04-05 11:00:30', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2024-04-04 11:00:30', '2024-04-04 11:00:30'),
('MPoN7kq4GmRNegTXbl2f1kuCVRUCqj_7', '2024-04-05 11:05:25', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2024-04-04 11:05:25', '2024-04-04 11:05:25'),
('NYchQ8s9NG1KX0gmW1oycTI7NhipfONw', '2024-04-05 11:00:30', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2024-04-04 11:00:30', '2024-04-04 11:00:30'),
('vybWf9zm5SOy5LgwERdCNXlHYc1S3saY', '2024-04-05 11:00:30', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2024-04-04 11:00:30', '2024-04-04 11:00:30'),
('y7eN4k-GK2F7cgoTLI2QFNn-t3_ii_GF', '2024-04-05 11:25:44', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"adminId\":\"ebfb8691-460e-47d7-9667-b160ae4f55cf\"}', '2024-04-04 11:00:30', '2024-04-04 11:25:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `uuid`, `name`, `email`, `password`, `role`, `createdAt`, `updatedAt`) VALUES
(2, 'ebfb8691-460e-47d7-9667-b160ae4f55cf', 'admin', 'admin@gmail.com', '$argon2id$v=19$m=65536,t=3,p=4$FOf0LAFX9BIoIXa6YOutjg$L9b56r3jxm62ggwM0qz3asVVs/0UvrfrUBCTtffg+iE', 'admin', '2024-03-25 08:41:19', '2024-03-25 08:41:19');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `alat`
--
ALTER TABLE `alat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`) USING BTREE;

--
-- Indeks untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indeks untuk tabel `heros`
--
ALTER TABLE `heros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indeks untuk tabel `layanans`
--
ALTER TABLE `layanans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indeks untuk tabel `profils`
--
ALTER TABLE `profils`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indeks untuk tabel `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sid`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `alat`
--
ALTER TABLE `alat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT untuk tabel `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `heros`
--
ALTER TABLE `heros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `layanans`
--
ALTER TABLE `layanans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `profils`
--
ALTER TABLE `profils`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `alat`
--
ALTER TABLE `alat`
  ADD CONSTRAINT `alat_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD CONSTRAINT `berita_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `heros`
--
ALTER TABLE `heros`
  ADD CONSTRAINT `heros_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `layanans`
--
ALTER TABLE `layanans`
  ADD CONSTRAINT `layanans_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `profils`
--
ALTER TABLE `profils`
  ADD CONSTRAINT `profils_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
